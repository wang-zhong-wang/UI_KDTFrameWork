# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/23 17:26

from tools.createReport import CreateReport
from tools.operateConfig import OperateConfig
from tools.executeCase import *
from tools.timeTools import TimeTools
from tools.resultStatistics import *
from tools.sendEmail import SendEmail
from tools.checkDriverVersion import CheckDriverVersion
from tools.checkRequirements import CheckRequirements

from config.filePathConfig import configPath

cf = OperateConfig().read_config_data(configPath)

title = cf.get("report", "title")
description = cf.get("report", "description")


def start_run(threadNum=1, fileName=None):
    """
    执行所有测试用例，写入测试结果到excel报告中
    :param threadNum: 启动的线程数量
    :param fileName: 传入用例文件名称只执行一个文件，默认执行全部文件用例
    :return:
    """
    # 检测本地浏览器驱动
    # CheckDriverVersion().check_driver_version()
    # CheckRequirements().check_requirements()
    start_time = TimeTools.get_now_date()
    # 初始化测试统计数据
    result_init()
    # 执行测试用例集
    execute_all_case(threadNum=threadNum, caseFileName=fileName)
    end_time = TimeTools.get_now_date()
    # 写入测试数据生成测试报告
    CreateReport().create_report(title=title, description=description, startTime=start_time, endTime=end_time)
    # 发送测试报告
    SendEmail().send_email()
	
	
# 本地git冲突解决测试代码

# git 代码冲突解决测试

if __name__ == '__main__':
    start_run(threadNum=2)
