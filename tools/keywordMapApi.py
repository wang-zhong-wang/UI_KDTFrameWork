# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/24 11:24

"""
字符串对应映射的函数方法
"""

from actions.driver import Driver
from actions import Base

sessionLs = {}


def key_word_map_api(rowDataList, threadNum):
    """
    字符串映射方法接口
    :param threadNum: 线程号，默认一个线程使用当前session
    :param rowDataList: 测试用例excel中行数据列表
    :return:
    """
    global driver, base, newValues

    keyWord, by, path, value = rowDataList[2], rowDataList[3], rowDataList[4], rowDataList[5]

    if keyWord is None:
        raise Exception(f"步骤：{rowDataList} 关键字不能为None！！！")

    # 有些关键字可能传多个参数，这里 | 符号为多参数区分加入列表
    value = value.split("|") if "|" in str(value) else [value]
    values = [by, path] + value
    # 删除列表中值为None的数据
    newValues = [v for v in values if v is not None]

    # 打开浏览器单独处理，返回session加入列表中，用于切换driver
    if keyWord == "open_browser":
        driver = Driver().open_browser(*newValues)
        # 线程号对应浏览器启动session对象
        sessionLs[str(threadNum)] = driver
    else:
        # 所有传入的参数必须对应函数的参数顺序
        getattr(Base(sessionLs[str(threadNum)]), keyWord)(*newValues)

    return driver
