# -*- coding: utf-8 -*-

# @Project : DDT_FrameWork
# @Author  : Mr.Deng
# @Time    : 2021/8/27 20:44

"""
.txt 文件读写
"""

from tools.logConfig import Logger

import os

Log = Logger().origin_logger


def read_txt_data(txtDataPath) -> list:
    """
    读取txt文件数据
    :param txtDataPath:
    :return:
    """
    with open(txtDataPath, "r", encoding="utf-8") as f:
        return [lineData.strip("\n") for lineData in f.readlines()]


def write_txt_data(txtDataPath, data: str or int):
    """
    txt文件中写入数据
    :param txtDataPath:
    :param data:
    :return:
    """
    with open(txtDataPath, "a", encoding="utf-8") as f:
        f.write(data)
    Log.info(f"文件：{txtDataPath} 中写入一行数据：{data}")
