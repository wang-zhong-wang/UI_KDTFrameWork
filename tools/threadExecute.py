# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/29 22:30

import threading
import time


def run_by_thread(parseExcel, caseList, threadNum, funcName):
    """
    多线程运行测试用例
    :param caseList: 用例名称列表
    :param threadNum: 多线程启动数量，不能大于用例列表总数
    :param funcName: 运行功能名称
    :return:
    """

    # 判断剩余用例数，启动最大线程就等于剩余用例数
    if threadNum > len(caseList):
        threadNum = len(caseList)
    if threadNum > 2:
        raise Exception("** 线程数建议启动两个，多了同时写入excel数据会报错！！！")

    threadList = []

    for thread in range(threadNum):
        threadList.append(threading.Thread(target=funcName, args=(parseExcel, caseList[thread], thread)))

    for thread in threadList:
        time.sleep(2)  # 加个时间差异，去掉就是默认同时执行
        thread.start()

    for thread in threadList:
        time.sleep(2)
        thread.join()
