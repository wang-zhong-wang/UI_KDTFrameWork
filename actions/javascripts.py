# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/29 1:45

"""
使用js脚本，页面按像素值滚动、滚动到最底部、按可见元素位置滚动、删除元素属性、修改元素属性
"""

from actions.find import Find


class JavaScript:
    """
    ** 滚动方法不生效，后置增加sleep强制等待
    eg：
    1、页面滚动："window.scrollBy(0,500)"
    2、页面滚动到元素指定位置："arguments[0].scrollIntoView();", element
    3、页面滚动到最底部："window.scrollTo(0,document.body.scrollHeight);"
    4、打开另一个页面："window.open('http:www.xxx.com')"
    5、修改页面元素属性："arguments[0].removeAttribute('readonly');", element
    """

    def __init__(self, driver):
        self.find = Find(driver)
        self.driver = driver

    def use_js(self, js: str):
        """
        执行js脚本
        :param js: js脚本
        :return:
        """
        self.driver.execute_script(js)

    def scroll_page_by_pix(self, pix: str = 500):
        """
        页面滚动多少像素点
        :param pix:
        :return:
        """
        self.use_js(js=f"window.scrollBy(0, {pix})")

    def scroll_page_bottom(self):
        """
        滚动到页面最底部
        :return:
        """
        self.use_js(js="window.scrollTo(0,document.body.scrollHeight);")

    def scroll_page_arrive_element(self, by, path):
        """
        滚动到指定元素位置
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        arriveElement = self.find.find_element_presence(by, path)
        self.driver.execute_script("arguments[0].scrollIntoView();", arriveElement)

    def del_element_attribute(self, by, path, attributeName):
        """
        删除页面元素某一属性，输入框被禁用后可以修改属性来直接send_keys
        :param by: 定位方式
        :param path: 元素路径
        :param attributeName: 属性名称
        :return:
        """
        findElement = self.find.find_element_presence(by, path)
        self.driver.execute_script(f"arguments[0].removeAttribute('{attributeName}');", findElement)

    def set_element_attribute(self, by, path, attributeName):
        """
        修改页面元素某一属性
        :param by: 定位方式
        :param path: 元素路径
        :param attributeName: 属性名称
        :return:
        """
        findElement = self.find.find_element_presence(by, path)
        self.driver.execute_script(f"arguments[0].setAttribute('{attributeName}');", findElement)
