# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/29 15:57

"""
断言类关键字
"""

from actions.element import Element
from actions.browser import Browser

from tools.logConfig import Logger

Log = Logger().origin_logger


class Assertion:

    def __init__(self, driver):
        self.element = Element(driver)
        self.browser = Browser(driver)
        self.driver = driver

    def assert_current_title(self, title):
        """
        判断页面标题包含某字段
        :param title: 期望值
        :return:
        """
        currentTitle = self.browser.get_page_title()
        if title in currentTitle:
            Log.info(f"--- [断言标题] - 实际值：{currentTitle} 包含期望值：{title}，-> 测试通过")
        else:
            raise Exception(f"--- [断言标题] - 实际值：{currentTitle} 不包含期望值：{title}，-> 测试失败！！！")

    def assert_current_url(self, url):
        """
        判断当前页面url包含某字段
        :param url: 期望值
        :return:
        """
        currentUrl = self.browser.get_current_url()
        if url in currentUrl:
            Log.info(f"--- [断言地址] - 实际值：{currentUrl} 包含期望值：{url}，-> 测试通过")
        else:
            raise Exception(f"--- [断言地址] - 实际值：{currentUrl} 不包含期望值：{url}，-> 测试失败！！！")

    def assert_element_text(self, by, path, text):
        """
        判断当前页面元素某文本属性
        :param by:
        :param path:
        :param text: 期望值
        :return:
        """
        getText = self.element.get_text(by, path)
        if getText == text:
            Log.info(f"--- [断言文本] - 实际值：{getText} 等于期望值：{text}，-> 测试通过")
        else:
            raise Exception(f"--- [断言文本] - 实际值：{getText} 不等于期望值：{text}，-> 测试失败！！！")

    def assert_element_attribute(self, by, path, name, text):
        """
        判断页面元素属性值
        :param by:
        :param path:
        :param name: 索要获取的元素属性名称
        :param text: 期望得到的属性值
        :return:
        """
        getAttribute = self.element.get_attribute_value(by, path, name)
        if getAttribute == text:
            Log.info(f"--- [断言属性] - 实际值：{getAttribute} 等于期望值：{text}，-> 测试通过")
        else:
            raise Exception(f"--- [断言属性] - 实际值：{getAttribute} 不等于期望值：{text}，-> 测试失败！！！")

    def assert_element_is_displayed(self, by, path):
        """
        判断页面元素是否在页面上显示
        :param by:
        :param path:
        :return:
        """
        isHave = self.element.get_is_displayed(by, path)
        if isHave:
            Log.info(f"--- [断言可见] - 当前页面元素：{path} 可以看见，-> 测试通过")
        else:
            raise Exception(f"--- [断言可见] - 当前页面没有找到元素：{path} 不可见，-> 测试失败！！！")

    def assert_element_is_select(self, by, path):
        """
        判断页面元素是否已经被选中
        :param by:
        :param path:
        :return:
        """
        isSelect = self.element.get_is_selected(by, path)
        if isSelect:
            Log.info(f"--- [断言已选] - 当前页面元素：{path} 已被选中，-> 测试通过")
        else:
            raise Exception(f"--- [断言已选] - 当前页面元素：{path} 没有被选中，-> 测试失败！！！")

    def assert_element_is_enable(self, by, path):
        """
        判断页面元素是否可用可以点击。输入
        :param by:
        :param path:
        :return:
        """
        isEnable = self.element.get_is_enabled(by, path)
        if isEnable:
            Log.info(f"--- [断言可用] - 当前页面元素：{path} 可以使用，-> 测试通过")
        else:
            raise Exception(f"--- [断言可用] - 当前页面元素：{path} 不能使用，-> 测试失败！！！")
