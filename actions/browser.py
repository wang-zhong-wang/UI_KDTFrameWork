# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/23 18:07

"""
浏览器相关操作封装
"""

from tools.timeTools import TimeTools
from tools.operateConfig import OperateConfig

from config.filePathConfig import *

cf = OperateConfig()


class Browser:

    def __init__(self, driver):
        self.driver = driver

    def visit_url(self, url, isNew=None):
        """
        打开地址
        :param isNew:
        :param url:
        :return:
        """
        if isNew == "new":
            self.driver.execute_script(f"window.open('{url}')")
        else:
            self.driver.get(url)

    def quite(self):
        """
        退出浏览器
        :return:
        """
        self.driver.quit()

    def refresh_browser(self):
        """
        刷新浏览器
        :return:
        """
        self.driver.refresh()

    def get_current_url(self) -> str:
        """
        获取当前页面url
        :return:
        """
        return self.driver.current_url

    def get_page_title(self) -> str:
        """
        获取页面标题
        :return:
        """
        return self.driver.title

    def back(self):
        """
        返回上一级页面
        :return:
        """
        self.driver.back()

    def forward_page(self):
        """
        前进一级页面
        :return:
        """
        self.driver.forward()

    def window_max_size(self):
        """
        浏览器最大化
        :return:
        """
        self.driver.maximize_window()

    def window_set_size(self, width, height):
        """
        设置浏览器尺寸大小
        :param height:
        :param width:
        :return:
        """
        self.driver.set_window_size(width, height)

    def screen_shot(self) -> str:
        """
        错误截图
        :return:
        """
        filename = errorPicPath + TimeTools.get_now_date("%Y%m%d%H%M%S") + ".png"
        self.driver.get_screenshot_as_file(filename=filename)
        return filename
