# -*- coding: utf-8 -*-

# @Project : DDT_FrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/16 23:45

"""
alert弹窗处理操作，接受、取消、输入、获取文本
"""

from actions.wait import Wait


class Alert:

    def __init__(self, driver):
        self.wait = Wait(driver)

    def accept_alert(self):
        """
        接受alert弹窗
        :return:
        """
        self.wait.wait_alert_is_present().accpet()

    def dismiss_alert(self):
        """
        取消alert弹窗
        :return:
        """
        self.wait.wait_alert_is_present().dismiss()

    def input_alert(self, value):
        """
        alert弹窗中输入字段
        :param value: 弹窗输入的参数
        :return:
        """
        self.wait.wait_alert_is_present().send_keys(value)

    def get_alert_text(self) -> str:
        """
        获取alert弹窗显示字段
        :return:
        """
        return self.wait.wait_alert_is_present().text
