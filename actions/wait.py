# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/23 20:06

"""
显式等待页面元素，等待元素是否可点击，iframe框架，alert弹窗，固定等待时间，隐式等待
"""

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import time


class Wait:

    def __init__(self, driver):
        self.driver = driver

    def wait_presence_of_element(self, by, path):
        """
        找到页面元素是都存在页面上，不关心是否可见
        :param by: 定位方式
        :param path: 元素路径
        :return: bool
        """
        message = f"当页面元素树中 10s 内没有找到对应元素：{path} ！！！"
        WebDriverWait(self.driver, 10, 1).until(EC.presence_of_element_located((by, path)), message)

    def wait_visibility_of_element(self, by, path):
        """
        找到页面元素是都存在页面上，且元素可见
        :param by:
        :param path:
        :return: bool
        """
        message = f"当前页面中 10s 内没有找到或不可见对应元素：{path} ！！！"
        WebDriverWait(self.driver, 10, 1).until(EC.visibility_of_element_located((by, path)), message)

    def wait_element_to_be_clickable(self, by, path):
        """
        等待页面元素出现判断元素是否可以点击
        :param by:
        :param path:
        :return:
        """
        message = f"当前页面中 10s 内没有找到或不可点击对应元素：{path} ！！！"
        WebDriverWait(self.driver, 10, 1).until(EC.element_to_be_clickable((by, path)), message)

    def wait_frame_to_be_available(self, by, path):
        """
        等待iframe框出现并且切换到iframe框架内
        :param by:
        :param path:
        :return:
        """
        message = f"当前页面中 10s 内没有找到iframe框架对应元素：{path} ！！！"
        WebDriverWait(self.driver, 10, 1).until(EC.frame_to_be_available_and_switch_to_it((by, path)), message)

    def wait_alert_is_present(self):
        """
        等待页面弹窗出现并切入弹窗内
        :return:
        """
        message = f"当前页面中 10s 内没有找到alert弹窗 ！！！"
        WebDriverWait(self.driver, 10, 1).until(EC.alert_is_present(), message)

    def sleep(self, seconds):
        """
        等待页面加载死等
        :param seconds: 固定等待时间
        :return:
        """
        time.sleep(int(seconds))

    def wait_in_time(self, seconds):
        """
        隐式等待页面加载几秒，几秒内等待页面加载，完成后跳过等待
        :param seconds: 超时等待时间
        :return:
        """
        self.driver.implicitly_wait(int(seconds))
