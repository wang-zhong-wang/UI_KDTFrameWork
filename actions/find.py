# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/23 20:08

"""
查找页面元素，查找一组元素，查找父元素路径下根据标签查找一组元素
"""

from actions.wait import Wait


class Find:

    def __init__(self, driver):
        self.wait = Wait(driver)
        self.driver = driver

    def find_element_visibility(self, by, path):
        """
        查找页面元素返回元素对象，且页面可见，用于页面操作相关方法
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        self.wait.wait_visibility_of_element(by, path)
        return self.driver.find_element(by, path)

    def find_element_presence(self, by, path):
        """
        查找页面元素返回元素对象，不关心页面是否可见，用于查找元素信息等
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        self.wait.wait_presence_of_element(by, path)
        return self.driver.find_element(by, path)

    def find_elements_presence(self, by, path):
        """
        查找一组元素返回元素对象列表
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        self.wait.wait_presence_of_element(by, path)
        return self.driver.find_elements(by, path)

    def find_elements_by_tag_name_(self, by, path, tagName):
        """
        当前父级元素目录下根据标签查找一组元素对象
        :param by: 定位方式
        :param path: 元素路径
        :param tagName: 标签名称
        :return:
        """
        parentElement = self.find_element_presence(by, path)
        return parentElement.find_elements_by_tag_name(tagName)
