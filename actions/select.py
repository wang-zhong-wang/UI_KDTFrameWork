# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/29 2:03

"""
Select 下拉框处理，包括：索引查找、文本查找、属性查找
"""

from selenium.webdriver.support.select import Select as _Select
from actions import Find


class Select:

    def __init__(self, driver):
        self.find = Find(driver)

    def select_by_text(self, by, path, text: str):
        """
        按页面显示文字选择下拉元素
        :param by: Select标签定位方式
        :param path: 元素路径
        :param text: 被选中的文本信息
        :return:
        """
        # 获取下拉框元素定位
        select = _Select(self.find.find_element_presence(by, path))
        select.select_by_visible_text(text)

    def select_by_index(self, by, path, index):
        """
        按页面显示索引选择下拉元素
        :param by: Select标签定位方式
        :param path: 元素路径
        :param index: 要选的索引
        :return:
        """
        # 获取下拉框元素定位
        select = _Select(self.find.find_element_presence(by, path))
        select.select_by_index(index)

    def select_by_value(self, by, path, value: str):
        """
        根据下拉列表选项中的标签的value属性值进行选择
        :param by: Select标签定位方式
        :param path: 元素路径
        :param value: 被选中的标签属性值
        :return:
        """
        # 获取下拉框元素定位
        select = _Select(self.find.find_element_presence(by, path))
        select.select_by_value(value)
