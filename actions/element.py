# -*- coding: utf-8 -*-

# @Project : KDT_FrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/16 23:00

"""
页面元素相关操作，点击、输入、清除、获取文本、属性、是否可见、是否已选、是否可点
"""

from selenium.webdriver.common.action_chains import ActionChains
from actions.find import Find


class Element:

    def __init__(self, driver):
        self.find = Find(driver)
        self.driver = driver

    def click(self, by, path):
        """
        点击页面元素按钮
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        self.find.find_element_visibility(by, path).click()

    def double_click(self, by, path):
        """
        鼠标左键双击按钮
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        element = self.find.find_element_visibility(by, path)
        ActionChains(self.driver).double_click(element).perform()

    def right_click(self, by, path):
        """
        右键点击按钮
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        el = self.find.find_element_visibility(by, path)
        ActionChains(self.driver).context_click(el).perform()

    def submit(self, by, path):
        """
        提交表单，此源码需要在一个表单（Form）中，并且type需要时submit类型
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        self.find.find_element_visibility(by, path).submit()

    def send_keys(self, by, path, value):
        """
        输入关键字
        :param by: 定位方式
        :param path: 元素路径
        :param value: 输入文本
        :return:
        """
        self.find.find_element_visibility(by, path).send_keys(value)

    def clear(self, by, path):
        """
        清除输入框数据
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        self.find.find_element_visibility(by, path).clear()

    def get_text(self, by, path) -> str:
        """
        获取元素文本信息
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        return self.find.find_element_presence(by, path).text

    def get_attribute_value(self, by, path, name) -> str:
        """
        获取页面元素属性信息
        :param by: 定位方式
        :param path: 元素路径
        :param name: 元素属性名称
        :return:
        """
        return self.find.find_element_presence(by, path).get_attribute(name)

    def get_is_displayed(self, by, path) -> bool:
        """
        获取页面元素是否可见
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        return self.find.find_element_presence(by, path).is_displayed()

    def get_is_selected(self, by, path) -> bool:
        """
        获取页面元素是否已选中
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        return self.find.find_element_presence(by, path).is_selected()

    def get_is_enabled(self, by, path) -> bool:
        """
        获取页面元素是否可点击，有些按钮置灰不能点击，eg：disabled="disabled"
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        return self.find.find_element_presence(by, path).is_enabled()
