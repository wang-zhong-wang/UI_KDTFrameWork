# UI_KDTFrameWork框架介绍

## 简介

    框架由Python3+selenium3+excel组成，关键字驱动

##  框架亮点

- 💗 测试完成后自动生成excel测试报告
- 💗 多线程执行测试用例，提高测试速度
- 💗 内置谷歌浏览器驱动版检测并自动下载
- 💗 内置项目运行依赖自动检测下载
- 💗 webdriver下方法分类并二次封装更方便调用
- 💗 智能等待页面元素加载告别 time.sleep 死等
- 💗 logging日志二次封装，全方位日志监控方便查找测试记录
- 💗 测试失败自动截图
- 💗 执行完成自动发送邮件，附测试报告、测试内容等


## 运行截图
![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/004636_3dad7476_7838265.png "微信截图_20210926003910.png")

## 测试报告
![输入图片说明](https://images.gitee.com/uploads/images/2021/1001/043154_815b34b3_7838265.png "屏幕截图.png")

## 框架结构

    actions: 关键字webdriver操作浏览器方法分类二次封装
        |——_init_.py 其下Base类继承所有操作类，方便调用
        |——mouse.py 鼠标相关操作方法，例如：长按、拖拽
        |——alert.py 浏览器警告框相关处理方法，例如：接受、取消
        |——find.py 查找页面元素，一组元素返回页面元素对象
        |——driver.py 打开浏览器操作，浏览器配置
        |——brower.py 浏览器相关操作封装，例如：打开url、刷新页面等
        |——element.py 页面元素操作方法，例如：点击、清除、输出、获取信息等
        |——frame.py 页面frame框架处理，例如：切入、切出
        |——javascript.py 执行js脚本，例如：页面滚动、修改元素属性等
        |——keys.py 键盘操作
        |——select.py 下拉框处理方法
        |——wait.py 页面元素加载等待的几种方法
        |——window.py 窗口切换
        |——coolkie.py 浏览器cookie相关操作
        |——assertion.py 断言关键字
    case: 测试用例数据存放文件夹
    config: 项目配置文件存放文件夹
        |——driver: 谷歌浏览器驱动存放目录
        |——browser.ini 浏览器相关配置
        |——driverPath.ini 谷歌浏览器驱动保存路径
        |——filePathConfig.py 项目中相关文件夹的相对路径
    result: 项目测试结果数据输出文件
        |——errorPic: 错误截图存放目录
        |——log: 执行日志存放目录
        |——report: 测试报告存放目录
        |——statistics.json 测试结果统计数据缓存文件
    tools: 项目公共方法存放文件
        |——checkDriverVersion.py 自动检测本地谷歌浏览器版本，下载对应版本的驱动
        |——checkRequirements.py 检测本地项目依赖安装，不匹配的自动下载
        |——createReport.py 创建测试excel测试报告模板，写入测试结果数据等
        |——executeCase.py 调用映射方法遍历测试用例excel执行测试用例
        |——keywordMapApi.py 关键字反射主要方法，测试数据中的关键字映射actions中封装的方法
        |——logConfig.py logging日志模块二次封装
        |——operateConfig.py ini配置文件读写
        |——operateTxt.py txt文件读写
        |——operateJson.py json文件读写
        |——parseExcel.py 测试用例excel测试数据读取解析
        |——resultStatistics.py 测试结果数据统计
        |——sendEmail.py 发送邮件方法
        |——threadExecute.py 多线程运行测试用例方法
        |——timeTools.py 时间格式处理文件
    config.ini 项目配置文件
    requirements 项目依赖
    run.py 项目运行入口

## 项目部署
- 1、拉取项目 git clone https://gitee.com/RampagingTestEngineers/UI_KDTFrameWork.git
- 2、安装python3配置环境变量官网下载：https://www.python.org/
- 3、安装pycharm官网下载：https://www.jetbrains.com/pycharm/
- 4、本地下载谷歌浏览器，及对应版本驱动，其他浏览器需再config->driverPath.ini文件中设置对应的浏览器驱动路径
- 5、解压后，pycharm中打开项目，设置对应的 interpreter
- 6、直接运行run.py文件，执行邮箱测试小demo

## 注意事项
- 本框架测试报告为简易的excel，建议使用pytest + allure
- openpyxl 库在 load 加载excel时如果手动异常退出，有时会损坏测试用例excel，暂时还未解决
- 多线程运行最多不要超过 3 个，多了同时写入excel数据会报错

## 用例编写规则

- excel中编写测试用例，用例模板中对应字段填写值，么有的不填写
- 测试用例中关键字对应方法中value栏中有对应多个值的使用 “|” 符号分开
- 测试用例编号一定要对应测试步骤的表名
- 编写用例前面一定了解actions各类中关键字的使用方法

## 关键字编写规则

- actions中重新创建的类，必须在 _init_.py 文件中继承
- 关键字反射方法中都是按数组顺序传值的，编写关键方法有元素定位方式和元素路径的参数必须排在第一顺序是，by, path, value

## 使用说明   

- 1、上面测试demo跑通后方可编写测试用例

- 2、excel中编写测试用例，用例模板中对应字段填写值

- 3、点击run.py直接运行，run方法中不指定文件名就直接默认运行case文件夹下所有用例

嘿嘿嘿

## 打赏~

 💗感觉还不错的话，顺便支付宝打个赏呗~

![打赏码](https://images.gitee.com/uploads/images/2021/0918/222732_9f2c94f4_7838265.png "支付宝")