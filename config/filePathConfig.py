# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/23 17:57

"""
项目所有文件夹路径配置文件
"""

import os

parentPath = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

casePath = os.path.join(parentPath, "case") + "/"
logPath = os.path.join(parentPath, "result", "log") + "\\"
reportPath = os.path.join(parentPath, "result", "report") + "/"
errorPicPath = os.path.join(parentPath, "result", "errorPic") + "\\"
driverSavePath = os.path.join(parentPath, "config", "driver") + "\\"
driverStartPath = os.path.join(parentPath, "config", "driverPath.ini")
configPath = os.path.join(parentPath, "config.ini")
browserConfig = os.path.join(parentPath, "config", "browser.ini")
requirementsPath = os.path.join(parentPath,"requirements")
statisticResultPath = os.path.join(parentPath, "result", "statistic.json")